"""Application level configs."""

from flask import Flask

from apis.views import crawler_api_app


def create_app():
    """App initialization."""
    app = Flask(__name__)
    app.register_blueprint(crawler_api_app)

    return app
