"""Manager file for runserver."""

import os
import sys

from application import create_app

from flask_script import (
    Manager,
    Server
)

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

app = create_app()
manager = Manager(app)

manager.add_command("runserver", Server(
    use_debugger=True,
    use_reloader=True,
    host=os.getenv('IP', '0.0.0.0'),
    port=int(os.getenv('PORT', 5001))
))


if __name__ == '__main__':
    manager.run()

