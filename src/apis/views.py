"""API views."""

import os

from flask import (
    Blueprint,
    request
)

from spider.conn.config import BASE_PATH

crawler_api_app = Blueprint('crawler_api_app', __name__)


@crawler_api_app.route('/crawl', methods=['POST'])
def crawl_tags():
    """Index view."""
    main_tag_name = request.form['tag_name']
    question_xpath = request.form['question_xpath']
    tags_xpath = request.form['tags_xpath']
    url_xpath = request.form['url_xpath']
    num_questions = int(request.form['num_questions'])

    # change path for spider execution
    spider_path = '/'.join([BASE_PATH, 'src/spider'])
    os.chdir(spider_path)

    # invoke scraper script
    script = (f"python stack_spider.py --question_xpath={question_xpath} "
              f"--tags_xpath={tags_xpath} --url_xpath={url_xpath} "
              f"--tag_name={main_tag_name} --num={num_questions}")

    os.system(script)

    return ('', 204)
