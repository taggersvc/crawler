
import click

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

from conn.db import insert_main_tag_name, insert_question_details


chrome_options = Options()
chrome_options.add_argument("--headless")

driver = webdriver.Chrome(ChromeDriverManager().install(),
                          chrome_options=chrome_options)


def init_driver(url):
    driver.get(url)

    return driver


def close_driver():
    driver.close()


def scrape_stack(url, question_xpath, tags_xpath, url_xpath, tag_id):
    base = '//div[@class="summary"]'

    for i in url:
        content = init_driver(i)

        questions = content.find_elements_by_xpath(base)

        for question in questions:
            title = question.find_element_by_xpath(question_xpath).text
            tags = ','.join(list(map(lambda tag: tag.text, question.
                            find_elements_by_xpath(tags_xpath))))
            url = question.find_element_by_xpath(url_xpath).get_attribute(
                'href').strip('https://stackoverflow.com')

            insert_question_details(title, url, tags, tag_id)


@click.command()
@click.option('--question_xpath', help='Xpath for question title.')
@click.option('--tags_xpath', help='Xpath for tags of question.')
@click.option('--url_xpath', help='Xpath for url of the questions.')
@click.option('--tag_name', help='Main tag name to collect questions for.')
@click.option('--num', help='Number of pages to visit.')
def get_params(question_xpath, tags_xpath, url_xpath, tag_name, num):
    tag_name = tag_name.split(',')

    for tag in tag_name:
        tag_id = insert_main_tag_name(tag)

        start_urls = [
            f'http://stackoverflow.com/questions/tagged/{tag}'
            f'?page={page}&sort=newest&pagesize=50'
            for page in range(0, int(num))
        ]

        scrape_stack(start_urls, question_xpath, tags_xpath, url_xpath, tag_id)

    close_driver()


if __name__ == '__main__':
    get_params()
