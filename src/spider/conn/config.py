
import os

BASE_PATH = os.getcwd()

DOCKER = False

if not DOCKER:
    DB_CONFIG = {
        'USER': 'airflow',
        'PASSWORD': 'airflow',
        'HOST': '0.0.0.0',
        'PORT': '5432',
        'DATABASE': 'taggerapi'
    }
else:
    DB_CONFIG = {
        'USER': 'postgres',
        'PASSWORD':'postgres',
        'HOST': 'db',
        'PORT': '5432',
        'DATABASE': 'postgres'
    }
