
import psycopg2

from .config import DB_CONFIG


def prep_conn():
    try:
        connection = psycopg2.connect(user=DB_CONFIG['USER'],
                                      password=DB_CONFIG['PASSWORD'],
                                      host=DB_CONFIG['HOST'],
                                      port=DB_CONFIG['PORT'],
                                      database=DB_CONFIG['DATABASE'])
        cursor = connection.cursor()

        return connection, cursor
    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)


connection, cursor = prep_conn()


def insert_main_tag_name(tag_name):
    # check and insert main tag name
    query = f"select * from tags_tag where tag_name='{tag_name}'"
    cursor.execute(query)
    result = cursor.fetchall()

    if not result:
        query = f"insert into tags_tag (tag_name) values ('{tag_name}')"
        cursor.execute(query)
        connection.commit()
    else:
        print(f'Tag {tag_name} exists!')

    # getting respective id of the latest inserted main tag name
    query = f"select id from tags_tag where tag_name='{tag_name}'"
    cursor.execute(query)
    result = cursor.fetchone()

    return result[0]


def insert_question_details(title, url, tags, tag_id):
    title = title.replace("'", '')
    query = f"select * from tags_question where question='{title}'"
    cursor.execute(query)
    result = cursor.fetchall()

    if not result:
        title = title.replace("'", "''")
        url = url.replace("'", "''")
        query = f"insert into tags_question (question, url, tags, tag_id_id) " \
            f"values ('{title}', '{url}', '{tags}', {tag_id})"
        cursor.execute(query)
        connection.commit()
    else:
        print(f'Question {title} exists!')
